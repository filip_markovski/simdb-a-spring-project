/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author User
 */
@Entity(name = "Glumac")
@Table(name = "glumac")
public class Glumac implements Serializable  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int glumacID;

    @NotNull
    private String ime;

    @NotNull
    private String prezime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date datumRodjenja;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date datumSmrti;

    @NotNull
    private String drzava;

//    @ManyToMany
//    private Set<Film> filmovi = new HashSet<>();
    @OneToMany(
            mappedBy = "glumac",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Uloga> filmovi = new ArrayList<>();
    
    public Glumac() {
    }

    public Glumac(String ime, String prezime, Date datumRodjenja, Date datumSmrti, String drzava) {
        this.ime = ime;
        this.prezime = prezime;
        this.datumRodjenja = datumRodjenja;
        this.datumSmrti = datumSmrti;
        this.drzava = drzava;
    }

    /**
     * @return the glumacID
     */
    public int getGlumacID() {
        return glumacID;
    }

    /**
     * @param glumacID the glumacID to set
     */
    public void setGlumacID(int glumacID) {
        this.glumacID = glumacID;
    }

    /**
     * @return the ime
     */
    public String getIme() {
        return ime;
    }

    /**
     * @param ime the ime to set
     */
    public void setIme(String ime) {
        this.ime = ime;
    }

    /**
     * @return the prezime
     */
    public String getPrezime() {
        return prezime;
    }

    /**
     * @param prezime the prezime to set
     */
    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    /**
     * @return the datumRodjenja
     */
    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    /**
     * @param datumRodjenja the datumRodjenja to set
     */
    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    /**
     * @return the datumSmrti
     */
    public Date getDatumSmrti() {
        return datumSmrti;
    }

    /**
     * @param datumSmrti the datumSmrti to set
     */
    public void setDatumSmrti(Date datumSmrti) {
        this.datumSmrti = datumSmrti;
    }

    /**
     * @return the drzava
     */
    public String getDrzava() {
        return drzava;
    }

    /**
     * @param drzava the drzava to set
     */
    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    /**
     * @return the filmovi
     */
    public List<Uloga> getFilmovi() {
        return filmovi;
    }

    /**
     * @param filmovi the filmovi to set
     */
    public void setFilmovi(List<Uloga> filmovi) {
        this.filmovi = filmovi;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(getIme()).append(" ")
                .append(getPrezime()).toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.glumacID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Glumac other = (Glumac) obj;
        if (this.glumacID != other.glumacID) {
            return false;
        }
        if (!Objects.equals(this.ime, other.ime)) {
            return false;
        }
        if (!Objects.equals(this.prezime, other.prezime)) {
            return false;
        }
        return true;
    }

    
}

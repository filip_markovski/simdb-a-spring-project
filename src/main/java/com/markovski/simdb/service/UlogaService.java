/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service;

import com.markovski.simdb.domain.Uloga;
import com.markovski.simdb.domain.UlogaId;
import java.util.List;

/**
 *
 * @author User
 */
public interface UlogaService {
    List<Uloga> findAll();
    Uloga findById(UlogaId id);
    void save(Uloga uloga);
    void delete(int filmID);
    List<Uloga> findAllByMovie(int filmID);
}

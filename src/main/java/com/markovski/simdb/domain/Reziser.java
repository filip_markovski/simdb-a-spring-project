/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author User
 */
@Entity
@Table(name = "reziser")
public class Reziser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int reziserID;

    @NotNull
    private String ime;

    @NotNull
    private String prezime;

    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            },
            mappedBy = "reziser")
    private Set<Film> film = new HashSet<>();

    public Reziser() {
    }

    public Reziser(String ime, String prezime) {
        this.ime = ime;
        this.prezime = prezime;
    }

    /**
     * @return the reziserID
     */
    public int getReziserID() {
        return reziserID;
    }

    /**
     * @param reziserID the reziserID to set
     */
    public void setReziserID(int reziserID) {
        this.reziserID = reziserID;
    }

    /**
     * @return the ime
     */
    public String getIme() {
        return ime;
    }

    /**
     * @param ime the ime to set
     */
    public void setIme(String ime) {
        this.ime = ime;
    }

    /**
     * @return the prezime
     */
    public String getPrezime() {
        return prezime;
    }

    /**
     * @param prezime the prezime to set
     */
    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    /**
     * @return the filmovi
     */
    public Set<Film> getFilmovi() {
        return film;
    }

    /**
     * @param filmovi the filmovi to set
     */
    public void setFilmovi(Set<Film> film) {
        this.film = film;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(getIme()).append(" ")
                .append(getPrezime()).toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + this.reziserID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reziser other = (Reziser) obj;
        if (this.reziserID != other.reziserID) {
            return false;
        }
        if (!Objects.equals(this.ime, other.ime)) {
            return false;
        }
        if (!Objects.equals(this.prezime, other.prezime)) {
            return false;
        }
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.repository;

import com.markovski.simdb.domain.Uloga;
import com.markovski.simdb.domain.UlogaId;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author User
 */
@Repository
public interface UlogaRepository extends JpaRepository<Uloga, UlogaId> {
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service;

import com.markovski.simdb.domain.Glumac;
import java.util.List;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author User
 */
public interface GlumacService {
    List<Glumac> findAll();
    Glumac findById(int id);
    void save(Glumac glumac);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author User
 */
@Embeddable
public class UlogaId implements Serializable {
    @Column(name = "filmID")
    private int filmID;
    
    @Column(name = "glumacID")
    private int glumacID;

    public UlogaId() {
    }

    public UlogaId(int filmID, int glumacID) {
        this.filmID = filmID;
        this.glumacID = glumacID;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.filmID;
        hash = 17 * hash + this.glumacID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UlogaId other = (UlogaId) obj;
        if (this.filmID != other.filmID) {
            return false;
        }
        if (this.glumacID != other.glumacID) {
            return false;
        }
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service.impl;

import com.markovski.simdb.domain.Film;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.markovski.simdb.repository.FilmRepository;
import com.markovski.simdb.service.FilmService;

/**
 *
 * @author User
 */
@Service
@Transactional
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmRepository filmRepository;

    public FilmServiceImpl() {
    }
    
    @Override
    public List<Film> findAll() {
        return filmRepository.findAll();
    }

    @Override
    public Film findById(int id) {
        Optional<Film> optionalGlumac = filmRepository.findById(id);
        if (optionalGlumac.isPresent())
            return optionalGlumac.get();
        return null;
    }

    @Override
    public void save(Film movie) {
        filmRepository.save(movie);
    }

    @Override
    public void delete(int id) {
        filmRepository.deleteById(id);
    }
    
}

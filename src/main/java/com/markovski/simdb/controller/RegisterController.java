/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.controller;

import com.markovski.simdb.domain.Posetilac;
import com.markovski.simdb.service.PosetilacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author User
 */
@Controller
public class RegisterController {
    
    @Autowired
    private PosetilacService posetilacService;
        
    public RegisterController() {
    }
    
    @ModelAttribute(name = "posetilac")
    public Posetilac posetilac() {
        return new Posetilac("", "", "", "", 0);
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister() {
        return "/register";
    }

}

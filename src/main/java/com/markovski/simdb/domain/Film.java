/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.NaturalIdCache;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 *
 * @author User
 */
@Entity(name = "Film")
@Table(name = "film")
public class Film implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int filmID;
    
    @NotNull
    private String naslov;
    
    @NotNull
    private int godina;
    
    @NotNull
    private int trajanje;
    
    @NotNull
    @Column(length = 2000)
    private String opis;
    
    @NotNull
    private String posterFilma;
    
    @NotNull
    private String jezik;
    
    @ManyToOne
    @JoinColumn(name = "posetilacID")
    @NotNull
    private Posetilac posetilac;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "produkcija_film",
            joinColumns = @JoinColumn(name = "filmID"),
            inverseJoinColumns = @JoinColumn(name = "produkcijaID")
    )
    private Set<Produkcija> produkcija = new HashSet<>();
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "reziser_film",
            joinColumns = @JoinColumn(name = "filmID"),
            inverseJoinColumns = @JoinColumn(name = "reziserID")
    )
    private Set<Reziser> reziser = new HashSet<>();
    
    @OneToMany(
            mappedBy = "film",
            orphanRemoval = true
    )
    private List<Uloga> glumci = new ArrayList<>();
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "zanr_film",
            joinColumns = @JoinColumn(name = "filmID"),
            inverseJoinColumns = @JoinColumn(name = "zanrID")
    )
    private Set<Zanr> zanr= new HashSet<>();
    
    public Film() {
    }

    public Film(String naslov, int godina, int trajanje, String opis, String posterFilma, String jezik, Posetilac posetilac) {
        this.naslov = naslov;
        this.godina = godina;
        this.trajanje = trajanje;
        this.opis = opis;
        this.posterFilma = posterFilma;
        this.jezik = jezik;
        this.posetilac = posetilac;
    }

    /**
     * @return the filmID
     */
    public int getFilmID() {
        return filmID;
    }

    /**
     * @param filmID the filmID to set
     */
    public void setFilmID(int filmID) {
        this.filmID = filmID;
    }

    /**
     * @return the naslov
     */
    public String getNaslov() {
        return naslov;
    }

    /**
     * @param naslov the naslov to set
     */
    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    /**
     * @return the godina
     */
    public int getGodina() {
        return godina;
    }

    /**
     * @param godina the godina to set
     */
    public void setGodina(int godina) {
        this.godina = godina;
    }

    /**
     * @return the trajanje
     */
    public int getTrajanje() {
        return trajanje;
    }

    /**
     * @param trajanje the trajanje to set
     */
    public void setTrajanje(int trajanje) {
        this.trajanje = trajanje;
    }

    /**
     * @return the opis
     */
    public String getOpis() {
        return opis;
    }

    /**
     * @param opis the opis to set
     */
    public void setOpis(String opis) {
        this.opis = opis;
    }

    /**
     * @return the posterFilma
     */
    public String getPosterFilma() {
        return posterFilma;
    }

    /**
     * @param posterFilma the posterFilma to set
     */
    public void setPosterFilma(String posterFilma) {
        this.posterFilma = posterFilma;
    }
    
    /**
     * @return the jezik
     */
    public String getJezik() {
        return jezik;
    }

    /**
     * @param jezik the jezik to set
     */
    public void setJezik(String jezik) {
        this.jezik = jezik;
    }

    /**
     * @return the posetilac
     */
    public Posetilac getPosetilac() {
        return posetilac;
    }

    /**
     * @param posetilac the posetilac to set
     */
    public void setPosetilac(Posetilac posetilac) {
        this.posetilac = posetilac;
    }

    /**
     * @return the produkcije
     */
    public Set<Produkcija> getProdukcija() {
        return produkcija;
    }

    /**
     * @param produkcije the produkcije to set
     */
    public void setProdukcija(Set<Produkcija> produkcija) {
        this.produkcija = produkcija;
    }

    /**
     * @return the rezija
     */
    public Set<Reziser> getReziser() {
        return reziser;
    }

    /**
     * @param rezija the rezija to set
     */
    public void setReziser(Set<Reziser> reziser) {
        this.reziser = reziser;
    }

    /**
     * @return the glumci
     */
    public List<Uloga> getGlumci() {
        return glumci;
    }

    /**
     * @param glumci the glumci to set
     */
    public void setGlumci(List<Uloga> glumci) {
        this.glumci = glumci;
    }

    /**
     * @return the zanrovi
     */
    public Set<Zanr> getZanrovi() {
        return zanr;
    }

    /**
     * @param zanrovi the zanrovi to set
     */
    public void setZanrovi(Set<Zanr> zanrovi) {
        this.zanr = zanrovi;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(getNaslov()).append(", ")
                .append(getGodina()).append(", ")
                .append(getOpis()).toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.getFilmID();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Film other = (Film) obj;
        if (this.getFilmID() != other.getFilmID()) {
            return false;
        }
        return true;
    }
}

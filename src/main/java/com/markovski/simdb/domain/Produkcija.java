/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author User
 */
@Entity
@Table(name = "produkcija")
public class Produkcija implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int produkcijaID;

    @NotNull
    private String nazivProdukcije;

    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            },
            mappedBy = "produkcija")
    private Set<Film> film = new HashSet<>();

    public Produkcija() {
    }

    public Produkcija(String nazivProdukcije) {
        this.nazivProdukcije = nazivProdukcije;
    }

    /**
     * @return the produkcijaID
     */
    public int getProdukcijaID() {
        return produkcijaID;
    }

    /**
     * @param produkcijaID the produkcijaID to set
     */
    public void setProdukcijaID(int produkcijaID) {
        this.produkcijaID = produkcijaID;
    }

    /**
     * @return the nazivProdukcije
     */
    public String getNazivProdukcije() {
        return nazivProdukcije;
    }

    /**
     * @param nazivProdukcije the nazivProdukcije to set
     */
    public void setNazivProdukcije(String nazivProdukcije) {
        this.nazivProdukcije = nazivProdukcije;
    }

    /**
     * @return the filmovi
     */
    public Set<Film> getFilmovi() {
        return film;
    }

    /**
     * @param filmovi the filmovi to set
     */
    public void setFilmovi(Set<Film> film) {
        this.film = film;
    }

    @Override
    public String toString() {
        return getNazivProdukcije();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.getProdukcijaID();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produkcija other = (Produkcija) obj;
        if (this.getProdukcijaID() != other.getProdukcijaID()) {
            return false;
        }
        if (!Objects.equals(this.nazivProdukcije, other.nazivProdukcije)) {
            return false;
        }
        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 *
 * @author User
 */
@Entity(name = "Uloga")
@Table(name = "uloga")
public class Uloga implements Serializable {
    
    @EmbeddedId 
    private UlogaId id;
    
    @ManyToOne
    @MapsId("filmID")
    private Film film;
    
    
    @ManyToOne
    @MapsId("glumacID")
    private Glumac glumac;
    
    @NotNull
    private String nazivUloge;

    public Uloga() {
    }

    public Uloga(Film film, Glumac glumac, String nazivUloge) {
        this.film = film;
        this.glumac = glumac;
        this.nazivUloge = nazivUloge;
    }

    /**
     * @return the id
     */
    public UlogaId getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(UlogaId id) {
        this.id = id;
    }

    /**
     * @return the film
     */
    public Film getFilm() {
        return film;
    }

    /**
     * @param film the film to set
     */
    public void setFilm(Film film) {
        this.film = film;
    }

    /**
     * @return the glumac
     */
    public Glumac getGlumac() {
        return glumac;
    }

    /**
     * @param glumac the glumac to set
     */
    public void setGlumac(Glumac glumac) {
        this.glumac = glumac;
    }

    /**
     * @return the nazivUloge
     */
    public String getNazivUloge() {
        return nazivUloge;
    }

    /**
     * @param nazivUloge the nazivUloge to set
     */
    public void setNazivUloge(String nazivUloge) {
        this.nazivUloge = nazivUloge;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.getId());
        hash = 83 * hash + Objects.hashCode(this.film);
        hash = 83 * hash + Objects.hashCode(this.glumac);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Uloga other = (Uloga) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.film, other.film)) {
            return false;
        }
        if (!Objects.equals(this.glumac, other.glumac)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getNazivUloge();
    }
}

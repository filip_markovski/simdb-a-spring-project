/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service;

import com.markovski.simdb.domain.Film;
import java.util.List;

/**
 *
 * @author User
 */
public interface FilmService {
    List<Film> findAll();
    Film findById(int id);
    void save(Film film);
    void delete(int id);
}

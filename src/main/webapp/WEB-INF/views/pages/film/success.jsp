<%-- 
    Document   : success
    Created on : Sep 22, 2019, 2:51:18 PM
    Author     : User
--%>


<title>sIMDb</title>
<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ include file="../../common/header.jspf" %>

    <div id="wrap">
        
        <%@ include file="../../common/navigation.jspf" %>
        
        <div id="content" class="content">
            <div class="row d-flex vh-100">
            
                <div class="container align-self-center">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6 text-center">

                            <h1>Uspe�no ste dodali film!</h1>
                            <a href="get/${viewFilm.filmID}" class="btn btn-success">Prika�i film</a>

                        </div>
                    </div>
                </div>
            
            </div>
        </div>
        
        <%@ include file="../../common/footer.jspf" %>
        
    </div>
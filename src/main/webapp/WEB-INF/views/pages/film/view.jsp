<%-- 
    Document   : view
    Created on : Aug 31, 2019, 5:45:00 PM
    Author     : User
--%>

<title>sIMDb | Film</title>
<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ include file="../../common/header.jspf" %>

    <div id="wrap">
        
        <%@ include file="../../common/navigation.jspf" %>
        
        <div id="content" class="content">
            <div class="row d-flex mt-5 pt-5 pb-5">
            
                <div class="container align-self-center">
                    <div class="row justify-content-center">
                        
                        <div class="col-12 col-md-4">
                            <figure class="film-thumbnail">
                                <img src="${viewFilm.posterFilma}" alt="Poster filma" />
                            </figure>     
                        </div>
                        
                        <div class="col-12 col-md-8">
                            <h3>${viewFilm.naslov}</h3>
                            <p><span>${viewFilm.trajanje}</span> min | <span>zanrovi</span> | <span>${viewFilm.godina}</span></p>
                            <p>Re?ija:
                                <span>
                                    <c:forEach items="${viewFilm.reziser}" var="rez" varStatus="i">
                                        ${rez}
                                        <c:if test="${viewFilm.reziser.size() - i.index > 1}">
                                            ,
                                        </c:if>
                                    </c:forEach>
                                </span>
                            </p>
                            <p>Produkcija:
                                <span>
                                    <c:forEach items="${viewFilm.produkcija}" var="prod" varStatus="i">
                                        ${prod}
                                        <c:if test="${viewFilm.produkcija.size() - i.index > 1}">
                                            ,
                                        </c:if>
                                    </c:forEach>
                                </span>
                            </p>
                        </div>
                        
                    </div>
                            
                    <div class="row">
                        <div class="col-12">
                            <p class="text-justify">${viewFilm.opis}</p>
                        </div>
                    </div>
                            
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-dark">
                                <tbody>
                                    <c:forEach items="${viewFilm.glumci}" var="uloga">
                                        <tr>
                                            <td>${uloga.glumac}</td>
                                            <td>${uloga}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
        
        <%@ include file="../../common/footer.jspf" %>
        
    </div>
<%-- 
    Document   : edit
    Created on : Aug 31, 2019, 2:29:54 PM
    Author     : User
--%>

<title>sIMDb</title>
<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ include file="../../common/header.jspf" %>

    <div id="wrap">
        
        <%@ include file="../../common/navigation.jspf" %>
        
        <div id="content" class="content">
            <div class="row d-flex mt-5 pt-5 pb-4">

                <div class="container">
                    <div class="row flex-row justify-content-center">
                        <div class="col-md-12">
                            
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-primary"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default" >Dodaj film</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-dark" disabled="disabled"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default" >Dodaj glumce</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-dark" disabled="disabled"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default text-primar" >Dodaj re�iju</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-dark" disabled="disabled"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default" >Dodaj produkciju</p>
                                    </div>
                                </div>
                            </div>



                            <div class="row setup-content justify-content-center" id="step-1">
                                <div class="col-12">
                                    <h2 class="text-center mt-2 mb-3">Informacije o filmu</h2>
                                    <form:form method="POST" action="saveEdit" modelAttribute="film">
                                        <div class="row justify-content-center">
                                            <div class="col-12 col-md-6">

                                                
                                                <div class="form-row form-group justify-content-center mt-5">
                                                    <form:input type="text" placeholder="Naslov" path="naslov" value="${saveFilm.naslov}" class="form-control" />                                
                                                </div>

                                                <div class="form-row form-group justify-content-center">
                                                    <form:textarea placeholder="Opis" path="opis" rows="7" class="form-control"/>                       
                                                </div>
                                                
                                                <div class="form-row form-group justify-content-center">
                                                    <form:input type="text" placeholder="Link ka posteru filma" path="posterFilma" value="${saveFilm.posterFilma}" class="form-control" />                                
                                                </div>
                                                                    
                                            </div>
                                            <div class="col-12 col-md-6">

                                                <div class="form-row">
                                                    <div class="col-12 mt-3">
                                                        <form:label path="zanrovi">�anr:</form:label>

                                                        <c:forEach items="${saveFilm.zanrovi}" var="z">
                                                            <div class="form-group d-flex genre">
                                                                <form:select path="zanrovi" class="form-control mr-3" size="size" multiple="false" >
                                                                    <form:option value="0" label="Izaberite �anr" disabled="true" />
                                                                    
                                                                    <c:forEach items="${sviZanrovi}" var="zanr">
                                                                        <c:choose>
                                                                            <c:when test="${zanr == z}">
                                                                                <form:option value="${zanr.zanrID}" label="${zanr.nazivZanra}" selected="true" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <form:option value="${zanr.zanrID}" label="${zanr.nazivZanra}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:forEach>
                                                                              
                                                                </form:select>
                                                                <a href="#" class="btn btn-danger remove-field"><i class="fas fa-trash"></i></a>
                                                            </div>
                                                        </c:forEach>
                                                        
                                                        <c:forEach var="i" begin="${saveFilm.zanrovi.size()}" end="2">  
                                                            <div class="form-group d-flex genre">
                                                                
                                                                <form:select path="zanrovi" class="form-control mr-3" size="size" multiple="false" >
                                                                    <form:option value="0" label="Izaberite �anr" disabled="true" selected="true" />
                                                                    <c:forEach items="${sviZanrovi}" var="zanr">
                                                                        <form:option value="${zanr.zanrID}" label="${zanr.nazivZanra}" />
                                                                    </c:forEach>
                                                                </form:select>
                                                                <a href="#" class="btn btn-danger remove-field"><i class="fas fa-trash"></i></a>
                                                            
                                                            </div>
                                                        </c:forEach>
                                                       

                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <form:label path="trajanje">Trajanje:</form:label>
                                                            <form:input type="number" placeholder="Trajanje" path="trajanje" value="${saveFilm.trajanje}" class="form-control" />
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <form:label path="godina">Godina:</form:label>
                                                            <form:input type="number" placeholder="Godina" path="godina" value="${saveFilm.godina}" class="form-control" min="1900"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row">

                                                    <div class="col-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <%--<form:label path="jezik" class="mr-3 mb-0">Jezik: </form:label>--%>
                                                            <form:select path="jezik" class="form-control mr-3" size="size" multiple="false" >
                                                                <form:option value="0" label="Izaberite jezik" selected="true" disabled="true" />
                                                                <form:option value="bosanski" label="bosanski" />
                                                                <form:option value="crnogorski" label="crnogorski" />
                                                                <form:option value="engleski" label="engleski" />
                                                                <form:option value="hrvatski" label="hrvatski" />
                                                                <form:option value="makedonski" label="makedonski" />
                                                                <form:option value="slovena�ki" label="slovena�ki" />
                                                                <form:option value="srpski" label="srpski" />
                                                            </form:select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="text-center mar-t-35">
                                                    <button class="btn btn-dark" type="submit">Dalje</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form:form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>
        
        <!-- Actors Modal -->
        <div class="modal fade" id="actorsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Find an actor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="container-fluid">
                      
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-12">
                            
                            <div class="form-group has-search d-flex">
                                <span class="fas fa-search form-control-search"></span>
                                <input id="inputFilter" type="text" class="form-control icon mr-2" placeholder="Search" name="pretraga" onkeyup="searchFunction()" />
                            </div>

                            <div class="form-group">
                                <table id="tableFilter" class="table table-dark">
                                    <thead>
                                        <tr>
                                            <td>Glumac</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${glumci}" var="glumac">
                                            <tr>
                                                <td>${glumac}</td>
                                                <td>
                                                    <a href="glumac/get/${glumac.glumacID}" class="btn btn-success">Izaberi</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                      
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newActorModal">Add new actor</button>
              </div>
            </div>
          </div>
        </div>
        
        <!-- Add new actor Modal -->
        <div class="modal fade" id="newActorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add a new actor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="container-fluid">
                      
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-12">
                            
                            <div class="form-group">
                                
                                <form:form method="POST" action="save/glumac" modelAttribute="glumac">
                                    
                                    <div class="form-row justify-content-center">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <form:input type="text" placeholder="Ime" path="ime" class="form-control" />
                                                <form:errors path="ime"/>
                                            </div>
                                        </div>
                                    </div>
                                            
                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <form:input type="text" placeholder="Prezime" path="prezime" class="form-control" />
                                                <form:errors path="prezime"/>
                                            </div>
                                        </div>
                                    </div>
                                            
                                    <div class="form-row">
                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <form:label type="date" path="datumRodjenja" > Datum ro�enja </form:label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-8">
                                            <div class="form-group">
                                                <form:input type="date" placeholder="Datum ro�enja" path="datumRodjenja" class="form-control" />
                                                <form:errors path="datumRodjenja"/>
                                            </div>
                                        </div>
                                    </div>
                                            
                                    <div class="form-row">
                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <form:label type="date" path="datumSmrti" > Datum smrti </form:label>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-8">
                                            <div class="form-group">
                                                <form:input type="date" placeholder="Datum smrti" path="datumSmrti" class="form-control" />
                                                <form:errors path="datumSmrti"/>
                                            </div>
                                        </div>
                                    </div>
                                            
                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <form:input type="text" placeholder="Dr�ava" path="drzava" class="form-control" />
                                                <form:errors path="drzava"/>
                                            </div>
                                        </div>
                                    </div>
                                            
                                    <div class="form-row">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                            
                                </form:form>
                                
                            </div>
                            
                        </div>
                    </div>
                      
                  </div>
              </div>
                
            </div>
          </div>
        </div>
        
        <%@ include file="../../common/footer.jspf" %>
        <script src="<c:url value="/resources/js/removeField.js" />"></script>        
        <script src="<c:url value="/resources/js/search.js" />"></script>
        
        <!--<script src="<c:url value="/resources/js/modal.js" />"></script>-->
    </div>
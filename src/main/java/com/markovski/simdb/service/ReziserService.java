/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service;

import com.markovski.simdb.domain.Reziser;
import java.util.List;
import java.util.Set;

/**
 *
 * @author User
 */
public interface ReziserService {
    List<Reziser> findAll();
    Reziser findById(int id);
    void save(Reziser reziser);

}

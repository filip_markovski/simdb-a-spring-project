<%-- 
    Document   : add-4
    Created on : Sep 22, 2019, 12:02:00 PM
    Author     : User
--%>

<title>sIMDb</title>
<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ include file="../../common/header.jspf" %>

    <div id="wrap">
        
        <%@ include file="../../common/navigation.jspf" %>
        
        <div id="content" class="content">
            <div class="row d-flex mt-5 pt-5 pb-4">

                <div class="container">
                    <div class="row flex-row justify-content-center">
                        <div class="col-md-12">
                            
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-dark" disabled="disabled"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default" >Dodaj film</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-dark" disabled="disabled"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default" >Dodaj glumce</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-dark" disabled="disabled"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default text-primar" >Dodaj re�iju</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <button type="button" class="btn btn-circle btn-primary"></button>
                                        <p style="text-transform: uppercase; font-weight: bold;" class="text-default" >Dodaj produkciju</p>
                                    </div>
                                </div>
                            </div>



                            <div class="row setup-content justify-content-center" id="step-1">
                                <div class="col-12">
                                    <h2 class="text-center mt-2 mb-3">Informacije o filmu</h2>
                                    <div class="row justify-content-center">
                                        <div class="col-12 col-md-6">
                                            <form:form method="POST" action="produkcija/save" modelAttribute="producenti">

                                                <div class="row">
                                                    <div class="col-12 col-md-6">
                                                        <div class="form-row">

                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>Produkcija: </label>
                                                                    <!-- ime i prezime glumca -->
                                                                    <c:forEach items="${saveFilm.produkcija}" var="p">
                                                                        <div class="form-group d-flex flex-column">
                                                                            <form:input path="" readonly="true" value="${p}" class="form-control" />
                                                                            <a href="produkcija/delete/${p.produkcijaID}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                                        </div>
                                                                    </c:forEach>
                                                                    <!-- naziv uloge -->
                                                                    <!-- Button trigger modal -->
                                                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#producersModal">
                                                                      <i class="fas fa-plus"></i> &nbsp; Add
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="text-center mar-t-35">
                                                    <button class="btn btn-dark" type="submit">Sa�uvaj film</button>
<!--                                                    <br>
                                                    <a href="add-1" class="btn btn-link text-primary prevBtn" style="text-transform:none; font-weight: normal;">Nazad</a>-->
                                                </div>
                                                                    
                                            </form:form>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            
            </div>
        </div>
        
        <!-- Producers Modal -->
        <div class="modal fade" id="producersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Izaberite rezisera</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="container-fluid">
                      
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-12">
                            
                            <div class="form-group has-search d-flex">
                                <span class="fas fa-search form-control-search"></span>
                                <input id="inputFilter" type="text" class="form-control icon mr-2" placeholder="Search" name="pretraga" onkeyup="searchFunction()" />
                            </div>

                            <div class="form-group">
                                <table id="tableFilter" class="table table-dark">
                                    <thead>
                                        <tr>
                                            <td>Produkcija</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${sviProducenti}" var="produkcija">
                                            <tr>
                                                <td>${produkcija}</td>
                                                <td>
                                                    <a href="produkcija/get/${produkcija.produkcijaID}" class="btn btn-success">Izaberi</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                      
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newProducerModal">Dodaj novu produkciju</button>
              </div>
            </div>
          </div>
        </div>
        
        <!-- Add new director Modal -->
        <div class="modal fade" id="newProducerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Dodajte produkciju</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="container-fluid">
                      
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-12">
                            
                            <div class="form-group">
                                
                                <form:form method="POST" action="producent/save" modelAttribute="produkcija">
                                    
                                    <div class="form-row justify-content-center">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <form:input type="text" placeholder="Naziv produkcije" path="nazivProdukcije" class="form-control" />
                                                <form:errors path="nazivProdukcije"/>
                                            </div>
                                        </div>
                                    </div>
                                            
                                    <div class="form-row">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                            
                                </form:form>
                                
                            </div>
                            
                        </div>
                    </div>
                      
                  </div>
              </div>
                
            </div>
          </div>
        </div>
        
        <%@ include file="../../common/footer.jspf" %>
        <script src="<c:url value="/resources/js/removeField.js" />"></script>        
        <script src="<c:url value="/resources/js/search.js" />"></script>
        
        <!--<script src="<c:url value="/resources/js/modal.js" />"></script>-->
    </div>
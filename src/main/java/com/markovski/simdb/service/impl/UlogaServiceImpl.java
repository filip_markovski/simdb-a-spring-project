/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service.impl;

import com.markovski.simdb.domain.Uloga;
import com.markovski.simdb.domain.UlogaId;
import com.markovski.simdb.repository.UlogaRepository;
import com.markovski.simdb.service.UlogaService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
@Transactional
public class UlogaServiceImpl implements UlogaService {
    
    @Autowired
    private UlogaRepository ulogaRepository;

    public UlogaServiceImpl() {
    }

    @Override
    public List<Uloga> findAll() {
        return ulogaRepository.findAll();
    }

    @Override
    public Uloga findById(UlogaId id) {
        Optional<Uloga> optionalUloga = ulogaRepository.findById(id);
        if (optionalUloga.isPresent())
            return optionalUloga.get();
        return null;
    }

    @Override
    public void save(Uloga uloga) {
        ulogaRepository.save(uloga);
    }

    @Override
    public void delete(int filmID) {
        List<Uloga> uloge = ulogaRepository.findAll();
        for (Uloga uloga : uloge) {
            if (uloga.getFilm().getFilmID() == filmID) {
                ulogaRepository.delete(uloga);
            }
        }
    }

    @Override
    public List<Uloga> findAllByMovie(int filmID) {
        List<Uloga> uloge = ulogaRepository.findAll();
        List<Uloga> list = new ArrayList<>();
        for (Uloga uloga : uloge) {
            if (uloga.getFilm().getFilmID() == filmID) {
                list.add(uloga);
            }
        }
        return list;
    }
    
}

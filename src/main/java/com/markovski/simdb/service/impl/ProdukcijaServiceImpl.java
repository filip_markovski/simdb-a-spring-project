/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service.impl;

import com.markovski.simdb.domain.Produkcija;
import com.markovski.simdb.repository.FilmRepository;
import com.markovski.simdb.repository.ProdukcijaRepository;
import com.markovski.simdb.service.ProdukcijaService;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
@Transactional
public class ProdukcijaServiceImpl implements ProdukcijaService {

    @Autowired
    private ProdukcijaRepository produkcijaRepository;

    public ProdukcijaServiceImpl() {
    }
    
    @Override
    public List<Produkcija> findAll() {
        return produkcijaRepository.findAll();
    }

    @Override
    public Produkcija findById(int id) {
        Optional<Produkcija> optionalProdukcija = produkcijaRepository.findById(id);
        if (optionalProdukcija.isPresent())
            return optionalProdukcija.get();
        return null;
    }

    @Override
    public void save(Produkcija produkcija) {
        produkcijaRepository.save(produkcija);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service.impl;

import com.markovski.simdb.domain.Reziser;
import com.markovski.simdb.repository.ReziserRepository;
import com.markovski.simdb.service.ReziserService;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
@Transactional
public class ReziserServiceImpl implements ReziserService {

    @Autowired
    private ReziserRepository reziserRepository;

    public ReziserServiceImpl() {
    }
    
    @Override
    public List<Reziser> findAll() {
        return reziserRepository.findAll();
    }

    @Override
    public Reziser findById(int id) {
        Optional<Reziser> optionalReziser = reziserRepository.findById(id);
        if (optionalReziser.isPresent())
            return optionalReziser.get();
        return null;
    }

    @Override
    public void save(Reziser reziser) {
        reziserRepository.save(reziser);
    }
}

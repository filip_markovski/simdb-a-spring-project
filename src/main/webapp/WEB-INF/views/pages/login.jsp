<%-- 
    Document   : login
    Created on : Aug 29, 2019, 9:38:03 PM
    Author     : User
--%>

<title>sIMDb | Login</title>
<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ include file="../common/header.jspf" %>

    <div id="wrap">
        
        <%@ include file="../common/navigation.jspf" %>
        
        <div id="content" class="content">
            <div class="row d-flex vh-100">
            
                <div class="container align-self-center">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="logo-thumbnail ml-auto mr-auto">
                                <img src="<c:url value="/resources/img/SIMDb_rectangle.png" />" alt="SIMDb logo">
                            </div>
                            <form:form method="POST" action="/simdb/welcome" modelAttribute="posetilac">
                                <div class="form-row justify-content-center pt-4 pb-4 text-center">
                                    <p>
                                        <font color="red">${errorMessage}</font>
                                    </p>
                                    
                                    <div class="col-10 mb-2">
                                        <form:input type="text" placeholder="Korisnicko ime" path="username" class="form-control" />
                                    </div>

                                    <div class="col-10 mb-2">
                                        <form:input type="password" placeholder="Lozinka" path="password" class="form-control" />
                                    </div>

                                    <div class="col-10">
                                        <button type="submit" class="btn btn-primary mb-0">Login</button>
                                    </div>

                                    <div>
                                        <p class="mb-0">Nemate nalog?</p>
                                        <a href="register">Registrujte se</a>
                                    </div>
<!--                                    <div class="col-10 bg-danger p-3 mt-2">
                                        <p class="text-white text-center mb-0">Nema naloga sa tim podacima</p>
                                        <p class="text-white text-center mb-0">Uneta lozinka nije tačna</p>
                                        <p class="text-white text-center mb-0">Do�lo je do gre�ke prilikom izvr�avanja traÅ¾enog zahteva</p>
                                    </div>-->
                                    
                                </div>
                            </form:form>

                        </div>
                    </div>
                </div>
            
            </div>
        </div>
        
        <%@ include file="../common/footer.jspf" %>
        
    </div>
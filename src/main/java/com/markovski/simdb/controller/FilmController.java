/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.controller;

import com.markovski.simdb.domain.Film;
import com.markovski.simdb.domain.Glumac;
import com.markovski.simdb.domain.Posetilac;
import com.markovski.simdb.domain.Produkcija;
import com.markovski.simdb.domain.Reziser;
import com.markovski.simdb.domain.Uloga;
import com.markovski.simdb.domain.UlogaId;
import com.markovski.simdb.domain.Zanr;
import com.markovski.simdb.service.FilmService;
import com.markovski.simdb.service.GlumacService;
import com.markovski.simdb.service.ProdukcijaService;
import com.markovski.simdb.service.ReziserService;
import com.markovski.simdb.service.UlogaService;
import com.markovski.simdb.service.ZanrService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 *
 * @author User
 */
@Controller
@SessionAttributes(names = {"saveFilm", "listaUloga", "listaRezisera", "listaProducenata", "tip", "admin"})
@RequestMapping(value = "/film/*")
public class FilmController {
    
    @Autowired
    private FilmService filmService;
    
    @Autowired
    private GlumacService glumacService;
    
    @Autowired
    private ZanrService zanrService;
    
    @Autowired
    private ReziserService reziserService;
    
    @Autowired
    private ProdukcijaService produkcijaService;
    
    @Autowired
    private UlogaService ulogaService;
    
    @ModelAttribute(name = "film")
    public Film film() {
        return new Film("", 1900, 0, "", "", "", null);
    }
    
    @ModelAttribute(name = "reziser")
    public Reziser reziser() {
        return new Reziser();
    }
    
    @ModelAttribute(name = "rezija")
    public Set<Reziser> rezija() {
        return new HashSet<>();
    }
    
    @ModelAttribute(name = "produkcija")
    public Produkcija produkcija() {
        return new Produkcija();
    }
    
    @ModelAttribute(name = "producenti")
    public Set<Produkcija> producenti() {
        return new HashSet<>();
    }
    
    @ModelAttribute(name = "glumac")
    public Glumac glumac() {
        return new Glumac("", "", new Date(), null, "");
    }
    
    @ModelAttribute(name = "uloga")
    public Uloga uloga() {
        return new Uloga(null, null, "");
    }
    
    @ModelAttribute(name = "sviGlumci")
    public List<Glumac> sviGlumci() {
        List<Glumac> glumci = new ArrayList<>();
        glumci = glumacService.findAll();
        return glumci;
    }
    
    @ModelAttribute(name = "sviReziseri")
    public Set<Reziser> sviReziseri() {
        Set<Reziser> reziseri = new HashSet<>();
        reziseri.addAll(reziserService.findAll());
        return reziseri;
    }
    
    @ModelAttribute(name = "sviProducenti")
    public List<Produkcija> sviProducenti() {
        List<Produkcija> producenti = new ArrayList<>();
        producenti = produkcijaService.findAll();
        return producenti;
    }
    
    @ModelAttribute(name = "sviZanrovi")
    public List<Zanr> sviZanrovi() {
        List<Zanr> zanrovi = zanrService.findAll();
        return zanrovi;
    }
    
    @GetMapping("/all")
    public ModelAndView all() {
        List<Film> filmovi = filmService.findAll();
//        for (Film film : filmovi) {
//            System.out.println(film);
//        }
        ModelAndView modelAndView = new ModelAndView("/film/all");
        modelAndView.addObject("filmovi", filmovi);
        return modelAndView;
    }
    
    @RequestMapping(value = "get/{id}", method = RequestMethod.GET)
    public ModelAndView get(@PathVariable("id") int id, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("/film/view");
        Film film = filmService.findById(id);
        List<Uloga> uloge = ulogaService.findAllByMovie(film.getFilmID());
        film.setGlumci(uloge);
        modelAndView.addObject("viewFilm", film);
        return modelAndView;
    }
    
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteMovie(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        
        ModelAndView modelAndView = new ModelAndView("redirect:/film/all");
        Film film = filmService.findById(id);
        
        ulogaService.delete(id);
        film.getProdukcija().clear();
        film.getReziser().clear();
        film.getZanrovi().clear();

        filmService.save(film);
        filmService.delete(id);
        return modelAndView;
    }
    
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView add(HttpServletRequest request) {
        ModelAndView mav;
        Posetilac p = (Posetilac) request.getSession().getAttribute("posetilac");
        if (p == null) {
            mav = new ModelAndView("/unauthorized");
            mav.addObject("tip", "gost");
        } else {
            Posetilac korisnik = p;
            switch (korisnik.getRole()) {
                case 1:
                    mav = new ModelAndView("/film/add-1");
                    mav.addObject("tip", "admin");
                    mav.addObject("admin", korisnik);
                    request.getSession().setAttribute("tip", "admin");
                    mav.addObject("film", new Film());
                    break;
                case 2:
                    mav = new ModelAndView("/unauthorized");
                    request.getSession().setAttribute("tip", "korisnik");
                    mav.addObject("tip", "korisnik");
                    break;
                default:
                    mav = new ModelAndView("/unauthorized");
                    request.getSession().setAttribute("tip", "gost");
                    mav.addObject("tip", "gost");
                    break;
            }
        }
        return mav;
    }
    
    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("id") int id, HttpServletRequest request) {
        ModelAndView mav;
        Posetilac p = (Posetilac) request.getSession().getAttribute("posetilac");
        if (p == null) {
            mav = new ModelAndView("/unauthorized");
            mav.addObject("tip", "gost");
        } else {
            Posetilac korisnik = p;
            switch (korisnik.getRole()) {
                case 1:
                    Film film = filmService.findById(id);
                    List<Uloga> uloge = ulogaService.findAllByMovie(film.getFilmID());
                    film.setGlumci(uloge);
                    mav = new ModelAndView("/film/edit");
                    mav.addObject("tip", "admin");
                    mav.addObject("admin", korisnik);
                    
                    mav.addObject("saveFilm", film);
                    
                    List<Uloga> listaUloga = film.getGlumci();
                    Set<Reziser> listaRezisera = film.getReziser();
                    Set<Produkcija> listaProducenata = film.getProdukcija();
                    
                    mav.addObject("listaUloga", listaUloga);
                    mav.addObject("listaRezisera", listaRezisera);
                    mav.addObject("listaProducenata", listaProducenata);
                    
                    break;
                case 2:
                    mav = new ModelAndView("/unauthorized");
                    mav.addObject("tip", "korisnik");
                    break;
                default:
                    mav = new ModelAndView("/unauthorized");
                    mav.addObject("tip", "gost");
                    break;
            }
        }
        return mav;
    }
    
    @RequestMapping(value = "edit/saveEdit", method = RequestMethod.POST)
    public String saveEditMovie(HttpServletRequest request, @ModelAttribute("saveFilm") Film stareInfo, @ModelAttribute("film") Film film, @ModelAttribute("admin") Posetilac admin, final RedirectAttributes redirectAttributes) {

        Set<Zanr> zanrovi = new HashSet<>();
        for (Zanr zanr : film.getZanrovi()) {
            Zanr novi = zanrService.findById(Integer.parseInt(zanr.toString()));
            zanrovi.add(novi);
        }
        
        redirectAttributes.addFlashAttribute("posetilac", admin);
        
        stareInfo.setNaslov(film.getNaslov());
        stareInfo.setOpis(film.getOpis());
        stareInfo.setPosterFilma(film.getPosterFilma());
        stareInfo.setZanrovi(zanrovi);
        stareInfo.setTrajanje(film.getTrajanje());
        stareInfo.setGodina(film.getGodina());
        stareInfo.setJezik(film.getJezik());
        stareInfo.setPosetilac(admin);

        redirectAttributes.addFlashAttribute("saveFilm", stareInfo);
        
        for (Uloga uloga : stareInfo.getGlumci()) {
            ulogaService.save(uloga);
        }

        return "redirect:/film/add-2";
    }
    
    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String saveMovie(HttpServletRequest request, @ModelAttribute("film") Film film, @ModelAttribute("admin") Posetilac admin, final RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("naslov", film.getNaslov());
        redirectAttributes.addFlashAttribute("opis", film.getOpis());
        Set<Zanr> zanrovi = new HashSet<>();
        for (Zanr zanr : film.getZanrovi()) {
            Zanr novi = zanrService.findById(Integer.parseInt(zanr.toString()));
            zanrovi.add(novi);
        }
        film.setZanrovi(zanrovi);
        redirectAttributes.addFlashAttribute("zanrovi", zanrovi);
        redirectAttributes.addFlashAttribute("trajanje", film.getTrajanje());
        redirectAttributes.addFlashAttribute("godina", film.getGodina());
        redirectAttributes.addFlashAttribute("jezik", film.getJezik());
        System.out.println("Posetilac: " + admin);
        redirectAttributes.addFlashAttribute("posetilac", admin);
        film.setPosetilac(admin);
        redirectAttributes.addFlashAttribute("saveFilm", film);
        redirectAttributes.addFlashAttribute("listaUloga", new ArrayList<Uloga>());
        redirectAttributes.addFlashAttribute("listaRezisera", new HashSet<Reziser>());
        redirectAttributes.addFlashAttribute("listaProducenata", new HashSet<Produkcija>());
        
        return "redirect:/film/add-2";
    }
    
    @RequestMapping(value = "add-2", method = RequestMethod.GET)
    public ModelAndView showActors(@ModelAttribute("saveFilm") Film film) {

        ModelAndView mav = new ModelAndView("/film/add-2");
        
//        System.out.println("showMovieInfo:");
//        System.out.println(film);
        for (Uloga uloga : film.getGlumci()) {
            System.out.println(uloga.getGlumac());
        }
        return mav;
    }
    
    @RequestMapping(value = "add-3", method = RequestMethod.GET)
    public ModelAndView showDirector(HttpServletRequest request, @ModelAttribute("saveFilm") Film film) {
        ModelAndView mav = new ModelAndView("/film/add-3");       
        System.out.println("Prave uloge:");
        for (Uloga uloga : film.getGlumci()) {
            System.out.println(uloga.getGlumac() + ": " + uloga.getNazivUloge());
        }
        return mav;
    }
    
    @RequestMapping(value = "add-4", method = RequestMethod.GET)
    public ModelAndView showProducer(@ModelAttribute("saveFilm") Film film) {

        ModelAndView mav = new ModelAndView("/film/add-4");      
        System.out.println("Posetilac: " + film.getPosetilac());

        return mav;
    }
    
    @RequestMapping(value = "success", method = RequestMethod.GET)
    public ModelAndView showSuccess(HttpServletRequest request, @ModelAttribute("saveFilm") Film film) {

        ModelAndView mav = new ModelAndView("/film/success");   
        mav.addObject("viewFilm", film);
        request.getSession().removeAttribute("saveFilm");
        request.getSession().removeAttribute("listaUloga");
        request.getSession().removeAttribute("listaRezisera");
        request.getSession().removeAttribute("listaProducenata");
        return mav;
    }
    
    @RequestMapping(value = "glumci/save", method = RequestMethod.POST)
    public String saveRoles(@ModelAttribute("saveFilm") Film film,
                            @ModelAttribute("uloga") Uloga uloga,
                             final RedirectAttributes redirectAttributes) {
        
        String[] nazivUloga = uloga.toString().split(",");
        int brojac = 0;
        for (Uloga uloga1 : film.getGlumci()) {
            uloga1.setNazivUloge(nazivUloga[brojac]);
            brojac++;
        }
        
        redirectAttributes.addFlashAttribute("saveFilm", film);
        return "redirect:/film/add-3";
    }
    
    @RequestMapping(value = "rezija/save", method = RequestMethod.POST)
    public String saveMovieDirectors(@ModelAttribute("saveFilm") Film film,
                             final RedirectAttributes redirectAttributes) {
        
        redirectAttributes.addFlashAttribute("saveFilm", film);
        return "redirect:/film/add-4";
    }
    
    @RequestMapping(value = "produkcija/save", method = RequestMethod.POST)
    public String saveProducers(@ModelAttribute("saveFilm") Film film, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult);
            return "redirect:/film/all";
        }
        
        filmService.save(film);
        
        for (Uloga uloga : film.getGlumci()) {
            uloga.setId(new UlogaId(film.getFilmID(), uloga.getGlumac().getGlumacID()));
            ulogaService.save(uloga);
        }
        
        return "redirect:/film/success";
    }
    
    @RequestMapping(value = "glumac/save", method = RequestMethod.POST)
    public String saveActor(@ModelAttribute("glumac") Glumac glumac,
                            @ModelAttribute("saveFilm") Film film,
                            @ModelAttribute("listaUloga") List<Uloga> listaUloga,
                             final RedirectAttributes redirectAttributes) {
        
        if (glumac != null) {
            glumacService.save(glumac);
            
            List<Glumac> listaZaProveru = new ArrayList<>();
            for (Uloga uloga : listaUloga) {
                listaZaProveru.add(uloga.getGlumac());
            }
            if (!listaZaProveru.contains(glumac)) {
                listaUloga.add(new Uloga(film, glumac, ""));
                film.setGlumci(listaUloga);
            }
        }
        System.out.println("Sacuvani glumac: " + glumac);
        return "redirect:/film/add-2";
    }
    
    @RequestMapping(value = "glumac/get/{id}", method = RequestMethod.GET)
    public String addActor(HttpServletRequest request,
                            @PathVariable("id") int id,
                            @ModelAttribute("saveFilm") Film film,
                            @ModelAttribute("listaUloga") List<Uloga> listaUloga,
                            final RedirectAttributes redirectAttributes) {

        Glumac glumacZaUbacivanje = glumacService.findById(id);
        
        List<Glumac> listaZaProveru = new ArrayList<>();
        for (Uloga uloga : listaUloga) {
            listaZaProveru.add(uloga.getGlumac());
        }
        if (glumacZaUbacivanje != null && !listaZaProveru.contains(glumacZaUbacivanje)) {
            listaUloga.add(new Uloga(film, glumacZaUbacivanje, ""));
        }
        
        film.setGlumci(listaUloga);
        System.out.println("Lista uloga iz 'glumac/get/id':");
        for (Uloga uloga : film.getGlumci()) {
            System.out.println(uloga.getGlumac() + ": " + uloga.getNazivUloge());
        }
        redirectAttributes.addFlashAttribute("saveFilm", film);                    
        return "redirect:/film/add-2";
    }
    
    @RequestMapping(value = "glumac/delete/{id}", method = RequestMethod.GET)
    public String removeActor(HttpServletRequest request,
                                @PathVariable("id") int id,
                                @ModelAttribute("saveFilm") Film film,
                                @ModelAttribute("listaUloga") List<Uloga> listaUloga,
                                final RedirectAttributes redirectAttributes) {

        Glumac glumacZaIzbacivanje = glumacService.findById(id);

        List<Glumac> listaZaProveru = new ArrayList<>();
        for (Uloga uloga : listaUloga) {
            listaZaProveru.add(uloga.getGlumac());
        }
        
        if (glumacZaIzbacivanje != null && listaZaProveru.contains(glumacZaIzbacivanje)) {
            for (Uloga uloga : listaUloga) {
                if (uloga.getGlumac().equals(glumacZaIzbacivanje)) {
                    listaUloga.remove(uloga);
                    break;
                }
            }
        }
        
        film.setGlumci(listaUloga);
        redirectAttributes.addFlashAttribute("saveFilm", film);  
        return "redirect:/film/add-2";
    }
    
    @RequestMapping(value = "reziser/save", method = RequestMethod.POST)
    public String saveDirector(@ModelAttribute("reziser") Reziser reziser,
                            @ModelAttribute("saveFilm") Film film,
                            @ModelAttribute("listaRezisera") Set<Reziser> listaRezisera,
                             final RedirectAttributes redirectAttributes) {
        
        if (reziser != null) {
            reziserService.save(reziser);

            if (!listaRezisera.contains(reziser)) {
                listaRezisera.add(reziser);
                film.getReziser().add(reziser);
            }
        }
        System.out.println("Sacuvani reziser: " + reziser);
        return "redirect:/film/add-3";
    }
    
    @RequestMapping(value = "rezija/get/{id}", method = RequestMethod.GET)
    public String addDirector(HttpServletRequest request,
                            @PathVariable("id") int id,
                            @ModelAttribute("saveFilm") Film film,
                            @ModelAttribute("listaRezisera") Set<Reziser> listaRezisera,
                            final RedirectAttributes redirectAttributes) {

        Reziser reziserZaUbacivanje = reziserService.findById(id);
                
        if (reziserZaUbacivanje != null && !listaRezisera.contains(reziserZaUbacivanje)) {
            listaRezisera.add(reziserZaUbacivanje);
        }
        
        film.setReziser(listaRezisera);
        System.out.println("Lista rezisera iz 'reziser/get/id':");
        for (Reziser reziser : listaRezisera) {
            System.out.println(reziser);
        }
        redirectAttributes.addFlashAttribute("saveFilm", film);                    
        return "redirect:/film/add-3";
    }
    
    @RequestMapping(value = "rezija/delete/{id}", method = RequestMethod.GET)
    public String removeDirector(HttpServletRequest request,
                                @PathVariable("id") int id,
                                @ModelAttribute("saveFilm") Film film,
                                @ModelAttribute("listaRezisera") Set<Reziser> listaRezisera,
                                final RedirectAttributes redirectAttributes) {

        Reziser reziserZaIzbacivanje = reziserService.findById(id);
        
        if (reziserZaIzbacivanje != null && listaRezisera.contains(reziserZaIzbacivanje)) {
            listaRezisera.remove(reziserZaIzbacivanje);
        }
        
        film.setReziser(listaRezisera);
        redirectAttributes.addFlashAttribute("saveFilm", film);  
        return "redirect:/film/add-3";
    }
    
    @RequestMapping(value = "producent/save", method = RequestMethod.POST)
    public String saveProducer(@ModelAttribute("produkcija") Produkcija produkcija,
                            @ModelAttribute("saveFilm") Film film,
                            @ModelAttribute("listaProducenata") Set<Produkcija> listaProducenata,
                             final RedirectAttributes redirectAttributes) {
        
        if (produkcija != null) {
            produkcijaService.save(produkcija);

            if (!listaProducenata.contains(produkcija)) {
                listaProducenata.add(produkcija);
                film.getProdukcija().add(produkcija);
            }
        }
        System.out.println("Sacuvana produkcija: " + produkcija);
        return "redirect:/film/add-4";
    }
    
    @RequestMapping(value = "produkcija/get/{id}", method = RequestMethod.GET)
    public String addProducer(HttpServletRequest request,
                            @PathVariable("id") int id,
                            @ModelAttribute("saveFilm") Film film,
                            @ModelAttribute("listaProducenata") Set<Produkcija> listaProducenata,
                            final RedirectAttributes redirectAttributes) {

        Produkcija produkcijaZaUbacivanje = produkcijaService.findById(id);
                
        if (produkcijaZaUbacivanje != null && !listaProducenata.contains(produkcijaZaUbacivanje)) {
            listaProducenata.add(produkcijaZaUbacivanje);
        }
        
        film.setProdukcija(listaProducenata);
        System.out.println("Lista producenata iz 'produkcija/get/id':");

        for (Produkcija produkcija : listaProducenata) {
            System.out.println(produkcija);
        }

        redirectAttributes.addFlashAttribute("saveFilm", film);                    
        return "redirect:/film/add-4";
    }
    
    @RequestMapping(value = "produkcija/delete/{id}", method = RequestMethod.GET)
    public String removeProducer(HttpServletRequest request,
                                @PathVariable("id") int id,
                                @ModelAttribute("saveFilm") Film film,
                                @ModelAttribute("listaProducenata") Set<Produkcija> listaProducenata,
                                final RedirectAttributes redirectAttributes) {

        Produkcija produkcijaZaIzbacivanje = produkcijaService.findById(id);
        
        if (produkcijaZaIzbacivanje != null && listaProducenata.contains(produkcijaZaIzbacivanje)) {
            listaProducenata.remove(produkcijaZaIzbacivanje);
        }
        
        film.setProdukcija(listaProducenata);
        redirectAttributes.addFlashAttribute("saveFilm", film);  
        return "redirect:/film/add-4";
    }
    
}

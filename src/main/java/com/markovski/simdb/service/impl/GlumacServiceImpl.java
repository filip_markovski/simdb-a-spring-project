/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service.impl;

import com.markovski.simdb.domain.Glumac;
import com.markovski.simdb.repository.GlumacRepository;
import com.markovski.simdb.service.GlumacService;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
@Transactional
public class GlumacServiceImpl implements GlumacService {
    
    @Autowired
    private GlumacRepository glumacRepository;

    public GlumacServiceImpl() {
    }

    @Override
    public List<Glumac> findAll() {
        return glumacRepository.findAll();
    }

    @Override
    public Glumac findById(int id) {
        Optional<Glumac> optionalGlumac = glumacRepository.findById(id);
        if (optionalGlumac.isPresent())
            return optionalGlumac.get();
        return null;
    }

    @Override
    public void save(Glumac glumac) {
        glumacRepository.save(glumac);
    }
}

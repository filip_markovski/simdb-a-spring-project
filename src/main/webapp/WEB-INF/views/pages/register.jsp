<%-- 
    Document   : register
    Created on : Aug 31, 2019, 5:47:41 PM
    Author     : User
--%>


<title>sIMDb | Register</title>
<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ include file="../common/header.jspf" %>

    <div id="wrap">
        
        <%@ include file="../common/navigation.jspf" %>
        
        <div id="content" class="content">
            <div class="row d-flex vh-100">
            
                <div class="container align-self-center">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="logo-thumbnail ml-auto mr-auto">
                                <img src="<c:url value="/resources/img/SIMDb_rectangle.png" />" alt="SIMDb logo">
                            </div>
                            <form:form method="POST" action="/simdb/korisnik/register" modelAttribute="posetilac">
                                <div class="form-row justify-content-center pt-4 pb-4 text-center">
                                    <p>
                                        <font color="red">${errorMessage}</font>
                                    </p>
                                    
                                    <div class="col-5 mb-2">
                                        <form:input type="text" placeholder="Ime" path="ime" class="form-control" />
                                    </div>
                                    
                                    <div class="col-5 mb-2">
                                        <form:input type="text" placeholder="Prezime" path="prezime" class="form-control" />
                                    </div>
                                    
                                    <div class="col-10 mb-2">
                                        <form:input type="text" placeholder="Korisnicko ime" path="username" class="form-control" />
                                    </div>

                                    <div class="col-10 mb-2">
                                        <form:input type="password" placeholder="Lozinka" path="password" class="form-control" />
                                    </div>

<!--                                    <div class="col-10">
                                        <button type="submit" class="btn btn-primary mb-0">Sa�uvaj</button>
                                        <button type="submit" class="btn btn-secondary btn-bordered mb-0">Odustani</button>
                                    </div>-->
                                    
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-primary mb-0">Registruj se</button>
                                    </div>

                                    <div>
                                        <p class="mb-0">Ve� posedujete nalog?</p>
                                        <p>Vrati se na <a href="/simdb/login">login.</a></p>
                                    </div>
<!--                                    <div class="col-10 bg-success p-3 mt-2">
                                        <p class="text-white text-center mb-0">Ve� postoji korisnik sa tim korisni�kim imenom.</p>
                                        <p class="text-white text-center mb-0">Sistem ne mo�e da registruje korisnika.</p>
                                        <p class="text-white text-center mb-0">Korisnik uspe�no a�uriran</p>
                                        <p class="text-white text-center mb-0">Korisnik ne mo�e da prika�e podatke o korisniku</p>
                                        <p class="text-white text-center mb-0">Sistem ne mo�e da izmeni korisnika</p>
                                    </div>-->
                                    
                                </div>
                            </form:form>

                        </div>
                    </div>
                </div>
            
            </div>
        </div>
        
        <%@ include file="../common/footer.jspf" %>
        
    </div>
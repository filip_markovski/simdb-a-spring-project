<%-- 
    Document   : all
    Created on : Aug 31, 2019, 2:29:54 PM
    Author     : User
--%>

<title>sIMDb | All Movies</title>
<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ include file="../../common/header.jspf" %>

    <div id="wrap">
        
        <%@ include file="../../common/navigation.jspf" %>
        
        <div id="content" class="content">
            <div class="row d-flex mt-5 pt-4 pb-4">
            
                <div class="container align-self-center">
                    <div class="row justify-content-center">
                        
                        <div class="form-group has-search d-flex">
                            <span class="fas fa-search form-control-search"></span>
                            <input id="inputFilter" type="text" class="form-control icon mr-2" placeholder="Search" name="pretraga" onkeyup="searchFunction()" />
                        </div>
                        
                        <div class="col-12">
                            
                            <table id="tableFilter" class="table table-dark table-hover">
                                <thead>
                                    <tr>
                                        <th>Film</th>
                                        <th>Godina</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <c:forEach items="${filmovi}" var="film"> 
                                        <tr>
                                            <td>${film.naslov}</td>
                                            <td>${film.godina}</td>
                                            <td>
                                                <a href="get/${film.filmID}" class="btn btn-info"><i class="fas fa-eye"></i> View</a>
                                                <c:if test="${tip == 'admin'}">
                                                    <a href="edit/${film.filmID}" class="btn btn-primary"><i class="fas fa-pen"></i> Edit</a>
                                                    <a href="delete/${film.filmID}" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                        
                                </tbody>
                            </table>

                            <div class="text-center">
                                <a href="add" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                            </div>
                            
                        </div>
                        
<!--                        <div class="col-8 bg-danger p-3 mt-2">
                            <p class="text-white text-center mb-0">Sistem je na?ao filmove po zadatoj vrednosti</p>
                            <p class="text-white text-center mb-0">Sistem ne mo?e da na?e filmove po zadatoj vrednosti</p>
                            <p class="text-white text-center mb-0">Podaci o izabranom filmu su uspe?no u�itani</p>
                            <p class="text-white text-center mb-0">Sistem je izmenio film</p>
                            <p class="text-white text-center mb-0">Sistem ne mo?e da prika?e podatke o filmu</p>
                            <p class="text-white text-center mb-0">Sistem ne mo?e da izmeni film</p>
                            <p class="text-white text-center mb-0">Sistem ne mo?e da obri?e film</p>
                        </div>-->
                        
                    </div>
                </div>
            
            </div>
        </div>
        
        <%@ include file="../../common/footer.jspf" %>
        <script src="<c:url value="/resources/js/search.js" />"></script>
        
    </div>
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markovski.simdb.service.impl;

import com.markovski.simdb.domain.Zanr;
import com.markovski.simdb.repository.ZanrRepository;
import com.markovski.simdb.service.ZanrService;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
@Transactional
public class ZanrServiceImpl implements ZanrService {
    
    @Autowired
    private ZanrRepository zanrRepository;

    public ZanrServiceImpl() {
    }

    @Override
    public List<Zanr> findAll() {
        return zanrRepository.findAll();
    }

    @Override
    public Zanr findById(int id) {
        Optional<Zanr> optionalZanr = zanrRepository.findById(id);
        if (optionalZanr.isPresent())
            return optionalZanr.get();
        return null;
    }

    @Override
    public void save(Zanr zanr) {
        zanrRepository.save(zanr);
    }
    
}
